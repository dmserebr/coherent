# Makefile for 'coherent' project
# TODO: enable optimization
CC=mpic++
CFLAGS=-c -std=c++11

all: coherent

coherent: coherent.o
	$(CC) coherent.o -o coherent

coherent.o: coherent.cpp
	$(CC) $(CFLAGS) coherent.cpp

mpitest:
	$(CC) $(CFLAGS) mpic++ mpitest.cpp -o mpitest

clean:
	rm coherent *o mpitest
