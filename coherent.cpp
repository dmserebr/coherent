#include <iostream>
#include <fstream>
#include <cmath>
#include <cstring>
#include <unistd.h>
#include <iomanip>
//#include <fftw3.h>

#include <mpi.h>

using namespace std;

// Global physical constants - everything is in CGS units

const double lambda = 0.00001; // 1 um
const double c_light = 3e10;
const double q_electron = 4.8e-10;
const double eps = 1e-12;
double omega0 = 2 * M_PI * c_light / lambda; 

// Problem parameters

double Rd = 0.1; // 1 mm
double alpha = 0.7; // ratio between max particle speed and c_light
double dy = 1e-3; // 10 um - was 0.1 um?
double dz = dy; // 10 um
int P = 128; // number of points from zero in X direction
int Q = P; // number of points from zero in Y direction
int Nt = 128; // time resolution
int n_threads = 1; // should be a divider of Nt
double Xmax; // calculated from alpha and omega0
double Ymax; // calculated from alpha and omega0
double Vmean; // dimensionless - fraction of c_light
double Vamp; // dimensionless - fraction of c_light

// Data arrays and shared values
int Nt_per_thread;
double dt;
double *Ax, *Ay;
double *Ax1, *Ay1; // used by one thread

bool verbose_log = false;

double traj_x(double t)
{
    return -Xmax * sin(2*omega0*t);
}

double traj_y(double t)
{
    return Ymax * cos(omega0*t);
}

double traj_vx(double t)
{
    return Xmax * (-2*omega0) * cos(2*omega0*t);
}

double traj_vy(double t)
{
    return Ymax * (-omega0) * sin(omega0*t);
}

//double omega0tau(double t)
//{
//cout << "Vmean = " << Vmean << "Vamp = " << Vamp << endl;
    //double V1 = sqrt(Vmean*Vmean - Vamp*Vamp); // TODO: don't recalculate every time
    //double tmp = 2 * atan(V1/(Vmean - Vamp) * tan(0.5 * V1 * c_light * t / Xmax));
    //int cnt = (int) (c_light * t + M_PI * Xmax / V1) / (2 * M_PI * Xmax / V1);
    //return tmp + cnt * 2 * M_PI;
//}

//double traj_x(double t)
//{
    //return -Xmax * sin(omega0tau(t));
//}

//double traj_y(double t)
//{
    //return Xmax * (-1.0 + cos(omega0tau(t)));
//}

double f_retard(double t1, double t, int p, double R0)
{
    return -c_light*t + c_light*t1 + R0 + Rd/R0 * traj_x(t1) + dy*p/R0 * traj_y(t1);
}

double find_root(double t, int p, int q, double R0)
{
    if (verbose_log)
    {
        cout << "find_root: t = " << t << ", p = " << p << ", q = " << q << ", R0 = " << R0 << endl;
    }
    double tmin = t - R0/c_light - 3 * 2*M_PI/omega0; // may be more accurate
    double tmax = t - R0/c_light + 3 * 2*M_PI/omega0;
    
    if (f_retard(tmin, t, p, R0) > 0)
    {
        cout << "Error - f_retard(tmin, t, p, R0) > 0 but it should be less than 0" << endl;
    }
    if (f_retard(tmax, t, p, R0) < 0)
    {
        cout << "Error - f_retard(tmax, t, p, R0) < 0 but it should be greater than 0" << endl;
    }
    
    double f_value, t1;
    bool found = false;
    int cnt = 0;
    while (!found)
    {
        t1 = (tmin + tmax)*0.5;
        f_value = f_retard(t1, t, p, R0);
        if (fabs(f_value) < eps)
        {
            if (verbose_log)
            {
                cout << "Root found: t1 = [" << t1 << "] after " << cnt << " iterations. t = " << t << ", p = " << p << ", q = " << q << endl;
            }
            found = true;
        }
        else if (f_value < 0)
        {
            tmin = t1;
        }
        else // f_value > 0
        {
            tmax = t1;
        }
        
        ++cnt;
        if (cnt > 1000)
        {
            cout << "Unable to find root after 1000 steps! Probably something went wrong..." << endl;
        }
    }
    return t1;
}

void write_field(double t, double ax, double ay)
{
    ofstream field_file("fields.txt", t > 0 ? ios::out | ios::app : ios::out);
    if (field_file.is_open())
    {
        field_file << t << '\t' << ax << '\t' << ay << endl;
    }
    else
    {
        cout << "Error - unable to open file" << endl;
    }
    field_file.close();
}

bool init_from_config(string filename)
{
    ifstream config_file(filename, ios::in);
    if (config_file.is_open())
    {
        cout << "Initializing from file " + filename + "..." << endl;
        string buf;
        while (getline(config_file, buf))
        {
            if (buf.find("#") == 0)
                continue;
            if (buf.find("Rd") == 0 && buf.find("=") == 3)
            {
                Rd = stod(buf.substr(4));
            }
            else if (buf.find("dy") == 0 && buf.find("=") == 3)
            {
                dy = stod(buf.substr(4));
            }
            else if (buf.find("dz") == 0 && buf.find("=") == 3)
            {
                dz = stod(buf.substr(4));
            }
            else if (buf.find("alpha") == 0 && buf.find("=") == 6)
            {
                alpha = stod(buf.substr(7));
            }
            else if (buf.find("P") == 0 && buf.find("=") == 2)
            {
                P = stoi(buf.substr(3));
            }
            else if (buf.find("Q") == 0 && buf.find("=") == 2)
            {
                Q = stoi(buf.substr(3));
            }
            else if (buf.find("Nt") == 0 && buf.find("=") == 3)
            {
                Nt = stoi(buf.substr(4));
            }
        }
        config_file.close();
        return true;
    }
    else 
    {
        cout << "Unable to open config file: " << filename << endl;
        return false;
    }
}

void log_config_values()
{
    cout << std::setprecision(19) << "Init from config: Rd = " << Rd << endl;
    cout << std::setprecision(19) << "Init from config: dy = " << dy << endl;
    cout << std::setprecision(19) << "Init from config: dz = " << dz << endl;
    cout << std::setprecision(19) << "Init from config: alpha = " << alpha << endl;
    cout << "Init from config: P = " << P << endl;
    cout << "Init from config: Q = " << Q << endl;
    cout << "Init from config: Nt = " << Nt << endl;
}

void threadFun(int thread_id)
{
    double t;     // 'global' t at the point where the fields are measured 
    double t1;    // 'local' t for particle
    double Vx, Vy;
    
    for (int t_i = thread_id * Nt_per_thread; t_i < (thread_id + 1) * Nt_per_thread && t_i < Nt; ++t_i)
    {
        cout << "Thread " << thread_id << ", step #" << t_i - thread_id * Nt_per_thread << " out of " << Nt_per_thread - 1 << endl;
        t = t_i * dt;
        
        for (int p=-P; p<=P; ++p)
        {
            for (int q=-Q; q<=Q; ++q)
            {
                //cout << "Rd = " << Rd << " dy = " << dy << " p = " << p << endl;
                double R0 = sqrt(Rd*Rd + dy*dy*p*p + dz*dz*q*q);
                //cout << "R0 = " << R0 << endl;
                t1 = find_root(t, p, q, R0);
                
                Vx = traj_vx(t1);
                Vy = traj_vy(t1);
                Ax1[t_i - thread_id * Nt_per_thread] += q_electron * Vx / c_light / R0;
                Ay1[t_i - thread_id * Nt_per_thread] += q_electron * Vy / c_light / R0;
            }
        }
    }
    
    if (thread_id > 0)
    {
        MPI::COMM_WORLD.Send(Ax1, Nt_per_thread, MPI_DOUBLE, 0, 1);
        MPI::COMM_WORLD.Send(Ay1, Nt_per_thread, MPI_DOUBLE, 0, 2);
    }
}

int main(int argc, char** argv)
{
    for (int i=0; i<argc; ++i)
    {
        auto arg = string(argv[i]);
        if (arg.compare("-v") == 0)
        {
            verbose_log = true;
        }
        else if (arg.find("-t") == 0)
        {
            n_threads = stoi(arg.substr(2));
        }
        else if (arg.find("conf") != string::npos)
        {
            if (!init_from_config(arg))
            {
                return 1;
            }
        }
    }
    
    MPI::Init();
    auto size = MPI::COMM_WORLD.Get_size();
    auto id = MPI::COMM_WORLD.Get_rank();
    
    if (id == 0)
    {
        log_config_values();
    }
    
    sleep(1); // let the user validate config
    
    Xmax = alpha * c_light * 0.5 / omega0;
    Ymax = alpha * c_light / omega0;
    
    if (id == 0)
    {
        Ax = new double[Nt];
        Ay = new double[Nt];
        memset(Ax, 0, Nt * sizeof(double));
        memset(Ay, 0, Nt * sizeof(double));
    }
    
    Nt_per_thread = (Nt % size == 0) ? Nt / size : Nt / (size - 1); // the last processor will handle the remainder
    Ax1 = new double[Nt_per_thread];
    Ay1 = new double[Nt_per_thread];
    memset(Ax1, 0, Nt_per_thread * sizeof(double));
    memset(Ay1, 0, Nt_per_thread * sizeof(double));
    
    double t_end = 2*M_PI / omega0; // exactly one period, maybe need to have 2-3 for sanity check
    dt = t_end / Nt;
    
    threadFun(id); // calculating slice of data
    
    if (size > 0 && id == 0)
    {
        for (int i=1; i<size; ++i)
        {
            cout << "Receiving from " << i << endl;
            double buf[Nt_per_thread];
            
            MPI::COMM_WORLD.Recv(buf, Nt_per_thread, MPI_DOUBLE, i, 1);
            cout << "Received Ax from " << i << endl;
            for (int j=0; j<Nt_per_thread && i*Nt_per_thread+j < Nt; ++j)
            {
                Ax[j + i*Nt_per_thread] = buf[j];
            }
            
            MPI::COMM_WORLD.Recv(buf, Nt_per_thread, MPI_DOUBLE, i, 2);
            cout << "Received Ay from " << i << endl;
            for (int j=0; j<Nt_per_thread && i*Nt_per_thread+j < Nt; ++j)
            {
                Ay[j + i*Nt_per_thread] = buf[j];
            }
        }
    }
    
    if (id == 0)
    {
        for (int j=0; j<Nt_per_thread; ++j)
        {
            Ax[j] = Ax1[j];
            Ay[j] = Ay1[j];
        }
        
        for (int i=0; i<Nt; ++i)
        {
            write_field(i, Ax[i], Ay[i]);
        }
    }
    
    //cout << "Performing Fourier transform for Ax..." << endl;
    
    ////int N = 65536;
    //fftw_complex *in, *out;
    //fftw_plan p;

    //in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * Nt);
    //out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * Nt);
    //p = fftw_plan_dft_1d(Nt, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    
    //for (int i=0; i<Nt; ++i)
    //{
        ////in[i][0] = sin(6*2*M_PI*i/N);
        //in[i][0] = Ax[i];
        //in[i][1] = 0;
    //}

    //fftw_execute(p); /* repeat as needed */

    //for (int i=0; i<10; ++i)
    //{
        //cout << out[i][0] << '\t' << out[i][1] << endl;
    //}
    
    //fftw_destroy_plan(p);
    //fftw_free(in); fftw_free(out);

    delete[] Ax;
    delete[] Ay;
    delete[] Ax1;
    delete[] Ay1;
    
    MPI::Finalize();
    if (id == 0)
    {
        cout << "Program ended" << endl;
    }
    return 0;
}
