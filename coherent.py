#!/usr/bin/env python3

import math
import multiprocessing

import matplotlib.pyplot as plt
import numpy as np
from decimal import Decimal
from joblib import Parallel, delayed
from scipy.fftpack import fft
from scipy.optimize import root
from scipy.integrate import quad
from scipy.special import kv

recalc = True
plot = True
log_scale = True
rand_points = True
plot_points = False

lmbda = 1e-4    # 1 micron
c_light = 3e10
q_electron = 4.8e-10 # should be < 0?

Vm = 0.99 * c_light   # V_mean
Va = 0.0 * c_light  # V_amplitude (modulation)

Rd = 0.1   # 1 mm
dy = 25 * 1e-7 # 100 nm
dz = dy
P = 144
Q = 0

y_displ = 0.0
z_displ = 0.0
Nt = 30000 # at 120k and v=0.99c, field envelope is relatively well resolved+
omega0 = 2 * math.pi * c_light / lmbda
R = Vm / omega0 # = (Vm / c_light) * lmbda / 2pi = 0.159 um
t_end = 2 * math.pi / omega0
dt = t_end / Nt

fit_degree = -0.79
fit_multiplier = 13e15


def traj_x(t):
    return R * np.sin(Vm * t / R + Va * np.sin(2 * omega0 * t) / 2 / omega0 / R)


def traj_y(t):
    return R * np.cos(Vm * t / R + Va * np.sin(2 * omega0 * t) / 2 / omega0 / R)


def traj_vx(t):
    return (Vm + Va * np.cos(2 * omega0 * t)) * \
           np.cos(Vm * t / R + Va * np.sin(2 * omega0 * t) / 2 / omega0 / R)


def traj_vy(t):
    return -1.0 * (Vm + Va * np.cos(2 * omega0 * t)) * \
           np.sin(Vm * t / R + Va * np.sin(2 * omega0 * t) / 2 / omega0 / R)


# this method does not tolerate rotation around z-axis
def f_retard(t1, t, y_coord, R0):
    return -c_light * t + c_light * t1 + R0 + Rd / R0  * traj_x(t1) + (y_coord + y_displ) / R0 * traj_y(t1)


def f_retard_right(t1, y_coord, R0):
    return R0 + Rd / R0  * traj_x(t1) + y_coord / R0 * traj_y(t1)


def calc_single(t_i, coords):
    print('t_i = {0} of {1}'.format(t_i, Nt))
    Ax2 = 0.0
    Ay2 = 0.0
    phi = 0.0
    t = t_i * dt

    for j in range(len(coords[1])):
        y_coord = coords[1][j]
        z_coord = coords[2][j]

        R0 = math.sqrt(Rd * Rd +  (y_coord + y_displ)**2 + (z_coord + z_displ)**2)
        # TODO: calculate R0 exactly to get phi
        t1_result = root(f_retard, np.array([t - R0 / c_light]), args=(t, y_coord, R0))
        t1 = t1_result['x'][0]
        # print('t1 = ' + str(t1))

        Vx = traj_vx(t1)
        Vy = traj_vy(t1)

        NVfactor = 1.0 + Rd / R0 * Vx / c_light + (y_coord + y_displ) / R0 * Vy / c_light
        Ax2 += q_electron * Vx / c_light / R0 / NVfactor
        Ay2 += q_electron * Vy / c_light / R0 / NVfactor

        phi += q_electron / R0 / NVfactor

    return Ax2, Ay2, phi


def gamma():
    return 1.0/math.sqrt(1.0 - ((Vm + Va) / c_light) ** 2)


def F(x):
    integral = quad(lambda y: kv(5.0/3, y), x, 10)
    return x * integral[0]


def Jackson(x, g, theta):
    # return x**2 * (kv(2.0/3, x))**2
    # return q_electron**2 * x**2 * R**2 * (kv(2.0/3, x * R / (3 * c_light * g**3)))**2 / (3 * math.pi**2 * c_light**3 * g**4)
    return x * np.sqrt(kv(2.0/3, x * R / (3 * c_light) * (1/g**2 + theta**2) ** 1.5) ** 2 +
                       (theta**2) / (theta**2 + 1/gamma**2) * kv(1.0/3, x * R / (3 * c_light) * (1/g**2 + theta**2) ** 1.5) ** 2)


t_arr = np.linspace(0, t_end, Nt, endpoint=False)

results = None

if Q == P:
    filename = 'coherent-data-p{0}-dy{1}-nt{2}{3}'.format(P, Decimal(round(dy*1e10)) / 1000, Nt,
                                                          '-rand' if rand_points else '')
else:
    filename = 'coherent-data-p{0}-q{1}-dy{2}-nt{3}{4}'.format(P, Q, Decimal(round(dy * 1e10)) / 1000, Nt,
                                                          '-rand' if rand_points else '')

n_points = (2*P + 1) * (2*Q + 1)
print('n_points = {}'.format(n_points))

x_coords = np.zeros(n_points)
y_coords = None
z_coords = None
if rand_points:
    np.random.seed(137)
    y_coords = np.random.rand(n_points) * (2*P*dy) - P*dy
    z_coords = np.random.rand(n_points) * (2*Q*dz) - Q*dz
else:
    y_coords = np.tile(np.linspace(-P*dy, P*dy, 2*P + 1), 2*Q + 1)
    z_coords = np.repeat(np.linspace(-Q*dz, Q*dz, 2*Q + 1), 2*P + 1)

if plot_points:
    plt.scatter(y_coords * 1e7, z_coords * 1e7)
    plt.title('Coordinates in nm')
    plt.show()

if recalc:
    num_cores = multiprocessing.cpu_count()
    results = Parallel(n_jobs=num_cores)(delayed(calc_single)(t_i, (x_coords, y_coords, z_coords)) for t_i in range(Nt))
    np.savetxt(filename, results)
else:
    results = np.loadtxt(filename)

gamma = gamma()
print('Gamma = {0}'.format(gamma))

print('omega0={0:e}'.format(omega0))
print('R={0}'.format(R))

Ax1 = np.array([result[0] for result in results])
Ay1 = np.array([result[1] for result in results])
phi = np.array([result[2] for result in results])

# plt.plot(t_arr, Ax1, '-', label='$A_x$')
# plt.plot(t_arr, Ay1, '-', label='$A_y$')
# plt.plot(t_arr, phi, '-', label=r'$\varphi$')
# plt.legend(loc='best')
# plt.show()


# Ax1d = Ax1
# Ay1d = Ay1
Ax1d = np.diff(np.append(Ax1, Ax1[0])) / dt
Ay1d = np.diff(np.append(Ay1, Ay1[0])) / dt

# plt.plot(t_arr, Ax1d, '.-', label='$A\'_x$')
# plt.plot(t_arr, Ay1d, '.-', label='$A\'_y$')
# plt.legend(loc='best')
# plt.show()



Ax1w = fft(Ax1d)
Ay1w = fft(Ay1d)

omega_arr = np.arange(0,len(t_arr))[:len(t_arr)//2]

Ax1wa = np.absolute(Ax1w)[:len(t_arr)//2] #* omega_arr
Ay1wa = np.absolute(Ay1w)[:len(t_arr)//2] #* omega_arr

Atotal = np.sqrt(Ax1wa**2 + Ay1wa**2)

# print('Argmax of Ax1wa = {0}'.format(np.argmax(Ax1wa)))
print('Argmax of Ay1wa = {0}'.format(np.argmax(Ay1wa)))

# plt.plot(omega_arr, Ax1wa, label=r'$A^x_\omega$')
if plot:
    plt.plot(omega_arr, Ay1wa, label=r'$A^y_\omega$')
Aymax = np.max(Ay1wa)
print("Aymax = {0}".format(Aymax))
# plt.plot(omega_arr, Atotal, label=r'$A_\omega$')

omega_crit = 1.5 * gamma**3
# plt.plot([0.29*omega_crit, 0.29*omega_crit], plt.ylim())
# plt.plot([omega_crit, omega_crit], plt.ylim(), label=r'$\omega_{crit}$')

omegas = omega_arr * omega0
# jackson = Jackson(omegas, gamma) * 4.3e-6 * 0.11
# print(omegas)
theta = math.atan2(z_displ, Rd)
print('theta = {0}'.format(theta))
jackson = Jackson(omegas, gamma, theta)
print(jackson)
argmax_j = np.argmax(jackson[1:])+1
print('Argmax of jackson = {0}'.format(argmax_j))

print("Jmax = {0}".format(np.max(jackson[1:])))

if plot:
    plt.plot(omega_arr, jackson * Aymax / np.max(jackson[1:]), label='Jackson')
    plt.xlim(-argmax_j, 10*argmax_j)
    plt.xlabel("Номер гармоники")
    plt.ylabel(r"$A_y$, пр. ед.")
# F_vals = np.array([F(omega1 / omega_crit) for omega1 in omega_arr])
# plt.plot(omega_arr, F_vals, label=r'analytic')

# plt.plot(omega_arr, 1e-4 * omega_arr**(-0.5), '--', label=r'$1/\omega^{1/2}$')
# plt.plot(omega_arr, 2e-5 * omega_arr**(-1.33), '--', label=r'$1/\omega^{4/3}$')
# plt.plot(omega_arr, 2e-5 * omega_arr**(-1.5), '-.', label=r'$1/\omega^{3/2}$')

    fit = omega_arr ** (fit_degree) * fit_multiplier
    plt.plot(omega_arr, fit, label='fit ({0}, {1:.1e})'.format(fit_degree, fit_multiplier))
    if log_scale:
        plt.xscale('log')
        plt.yscale('log')
    plt.xlim(1,None)
    plt.ylim(1e11, None)
    plt.legend(loc='best')


# plt.plot(t_arr, Ax1, '.-')
# plt.plot(t_arr, Ay1)
    plt.show()
