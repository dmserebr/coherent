#include <iostream>
#include "mpi.h"

int main()
{
    MPI::Init();
    int size = MPI::COMM_WORLD.Get_size();
    int id = MPI::COMM_WORLD.Get_rank();
    std::cout << "hi! id = " << id << std::endl;
    
    MPI::Finalize();
    return 0;
}
