#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np

fields = np.transpose(np.loadtxt('fields.txt'))
t = fields[0]
ax = fields[1]
ay = fields[2]

plt.plot(t,ax)
plt.plot(t,ay)

plt.show()
