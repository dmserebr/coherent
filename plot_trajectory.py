#!/usr/bin/env python3

# Plots circular trajectory with variable speed

import matplotlib.pyplot as plt
import numpy as np
import math

Va = 0.9  # dimensionless
Vm = 0.09 # dimensionless
R0 = 1e-4  # in CGS units
c_light = 3e10


def omega0tau(t):
    V1 = math.sqrt(Va*Va - Vm*Vm)
    tmp = 2 * np.arctan(V1/(Va - Vm) * np.tan(0.5 * V1 * c_light * t / R0))
    cnt = (c_light * t + math.pi * R0 / V1) // (2 * math.pi * R0 / V1)
    return tmp + cnt * 2 * math.pi


def X(t):
    return -R0 * np.sin(omega0tau(t))


def Y(t):
    return R0 * (-1.0 + np.cos(omega0tau(t)))


dt = 1e-16
t = np.arange(-1e-13, 1e-13, dt)
x = X(t)
y = Y(t)
Vx = np.diff(x) / dt
Vy = np.diff(y) / dt
V = np.sqrt(Vx * Vx + Vy * Vy)
g = 1 / np.sqrt(1 - V * V / c_light / c_light)

# plt.plot(t, omega0tau(t))
# plt.plot(t, x)
# plt.plot(t, y)
plt.plot(x, y)
# print(x)
# plt.plot(t[:-1], Vx)
# plt.plot(t[:-1], Vy)
# plt.plot(t[:-1], V)
# plt.plot(t[:-1], g)

plt.show()
